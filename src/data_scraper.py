import os

import pandas as pd
import numpy as np

from utils import TRAIN_SET_MINUTES


def prepare_data():
    # idx_dict = {
    #     "A  3": [[7, 8, 9], [4, 5, 6], [1, 2, 3, 31], [10, 11, 12]],
    #     "A  4": [[5, 6, 7, 8, 30], [4], [1, 2, 28], [9, 10, 11, 12, 31]],
    #     "A 22": [[6, 7], [3, 4, 5], [1, 2], [8, 9]],
    #     "A 23": [[8, 9, 10], [3, 4, 5], [1, 2], []],
    #     "A 45": [[7, 8, 9, 30], [4, 5, 6], [1, 2, 3], [10, 11, 12]],
    #     "A 98": [[5, 6], [3, 4], [1, 2], [7, 8]],
    #     "A 99": [[14, 15], [13], [11, 12], [16]],
    #     "A104": [[13, 14], [11, 12], [15, 16], [19, 20]],
    #     "A  2": [[], [4, 5, 6, 7], [], []],
    #     "A  5": [[], [], [], [5, 6, 14]],
    #     "A 12": [[6, 7, 8], [], [], []],
    #     "A 13": [[], [], [1, 2, 3], []],
    #     "A 46": [[4, 5], [], [], []],
    #     "A139": [[], [], [], [2]],
    #     "A163": [[], [3, 4], [], []],
    #     "A182": [[], [], [1, 2], []]
    # }

    idx_dict = {
        "A 12": [[6, 7, 8], [4, 5], [1, 2, 3], [9, 10]],
        "A 23": [[8, 9, 10], [3, 4, 5, 6, 7], [1, 2], []],
        "A 28": [[7, 8], [3, 4, 5, 6], [1, 2], []],
        "A 46": [[4, 5], [3], [1, 2], [6, 7]],
        "A 59": [[3], [2], [1], [4]],
        "A104": [[13, 14], [11, 12], [15, 16], [19, 20]],
        "A  3": [[], [], [1, 2, 3, 31], []],
        "A  4": [[], [], [1, 2, 28], []],
        "A  5": [[], [], [1, 2], []],
        "A 24": [[3, 4], [], [], []],
        "A 29": [[], [], [], []],
        "A 75": [[], [5, 6], [], []],
        "A110": [[], [], [], [4]],  # or 5
        "A126": [[4, 5], [], [], []],
        "A140": [[2], [], [], []],
        "A163": [[], [3, 4], [], []],

    }

    output_inters = [
        "A  2",
        "A  5",
        "A 12",
        "A 13",
        "A 46",
        "A139",
        "A163"
    ]

    filenames = os.listdir("../raw_traffic_data")

    for filename in filenames:
        df = pd.read_csv("../raw_traffic_data/"+filename, sep=';')
        # df.head()

        output_filename_raw = df.loc[[0], ["Time"]].to_string()[-5:]
        output_filename = output_filename_raw[:2] + "_" + output_filename_raw[3:]

        output_list = []

        for cross, dir_list in idx_dict.items():
            # out_file.write(cross+"\n")

            cross_list = [cross]
            cross_df = df.loc[df["Crossroad"] == cross]

            for idxs in dir_list:
                s = sum(cross_df.loc[cross_df["Sensor"].isin(idxs)]["Count"])
                # out_file.write(str(s)+"\n")
                cross_list.append(int(s))

            # out_file.write("\n")

            output_list.append(cross_list)

        # out_file = open("../processed_traffic_data/"+output_filename, 'w')

        pd.DataFrame(output_list).to_csv("../processed_traffic_data/"+output_filename, header=False, index=False)

        # out_file.close()

    # Second step of data transformation

    inter_filenames = os.listdir("../intersection_data")
    for inter in inter_filenames:
        try:
            os.remove("../intersection_data/"+inter)
        except Exception as e:
            print(e)

    filenames = os.listdir("../processed_traffic_data")

    for filename in filenames:
        df = pd.read_csv("../processed_traffic_data/"+filename, sep=',', header=None, index_col=0)
        df = df.fillna(0)

        for index, row in df.iterrows():
            file = open("../intersection_data/" + index, 'a')
            # if index == "A 12":
            #     file.write(str(list(row//4))[1:-1] + "\n")
                # print(index, end=' ')
            if index == "A 46":
                file.write(str(list(row*4))[1:-1] + "\n")
            elif index == "A 59":
                file.write(str(list(row*3))[1:-1] + "\n")
            # elif index == "A104":
            #     file.write(str(list(row*2))[1:-1] + "\n")
            # elif index == "A110":
            #     file.write(str(list(row+30))[1:-1] + "\n")
            # elif index == "A140":
            #     file.write(str(list(row+30))[1:-1] + "\n")
            else:
                file.write(str(list(row))[1:-1] + "\n")
            file.close()

    # z = pd.DataFrame(np.zeros((30, 4)))
    # z.to_csv("../intersection_data/A 28", header=False, index=False)

    # # creating A200 file
    # inter = pd.read_csv("../intersection_data/A 98", header=None).to_numpy()[:TRAIN_SET_MINUTES]
    # s = pd.read_csv("../intersection_data/A182", header=None).to_numpy()[:TRAIN_SET_MINUTES]
    # e = pd.read_csv("../intersection_data/A 45", header=None).to_numpy()[:TRAIN_SET_MINUTES]
    # n = pd.read_csv("../intersection_data/A 99", header=None).to_numpy()[:TRAIN_SET_MINUTES]
    # sum_in = inter.sum()
    # s_out = s.sum()
    # e_out = e.sum()
    # n_out = n.sum()
    #
    # w_out = max(sum_in - s_out - e_out - n_out, 0)
    #
    # w = pd.DataFrame([[0, w_out/TRAIN_SET_MINUTES, 0, 0] for _ in range(TRAIN_SET_MINUTES)])
    # w.to_csv("../intersection_data/A200", header=False, index=False)
    #
    # # creating AXXX file
    # inter = pd.read_csv("../intersection_data/A 22", header=None).to_numpy()[:TRAIN_SET_MINUTES]
    # s = pd.read_csv("../intersection_data/A 45", header=None).to_numpy()[:TRAIN_SET_MINUTES]
    # w = pd.read_csv("../intersection_data/A 99", header=None).to_numpy()[:TRAIN_SET_MINUTES]
    # n = pd.read_csv("../intersection_data/A  4", header=None).to_numpy()[:TRAIN_SET_MINUTES]
    # sum_in = inter.sum()
    # s_out = s.sum()
    # w_out = w.sum()
    # n_out = n.sum()
    #
    # e_out = max(sum_in - s_out - w_out - n_out, 0)
    #
    # e = pd.DataFrame([[0, 0, 0, e_out/TRAIN_SET_MINUTES] for _ in range(TRAIN_SET_MINUTES)])
    # e.to_csv("../intersection_data/AXXX", header=False, index=False)


# prepare_data()
