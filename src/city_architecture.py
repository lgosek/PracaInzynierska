from intersections import Intersection
from roads import Road
from edges import Edge

# array of road models - visible part of simulated roads
roads = [
    Road(x=5,   y=0,    grid_len=92, num_lanes=1, direction=1),     # 0
    Road(x=97,  y=2,    grid_len=92, num_lanes=1, direction=2),     # 1

    Road(x=102, y=3,    grid_len=52, num_lanes=2, direction=3),     # 2
    Road(x=98,  y=55,   grid_len=52, num_lanes=2, direction=4),     # 3

    Road(x=4,   y=3,    grid_len=52, num_lanes=2, direction=3),     # 4
    Road(x=0,   y=55,   grid_len=52, num_lanes=2, direction=4),     # 5

    Road(x=5,   y=56,   grid_len=92, num_lanes=1, direction=1),     # 6
    Road(x=97,  y=58,   grid_len=92, num_lanes=1, direction=2),     # 7

    Road(x=102, y=59,   grid_len=108, num_lanes=2, direction=3),    # 8
    Road(x=98,  y=167,  grid_len=108, num_lanes=2, direction=4),    # 9

    Road(x=4,   y=59,   grid_len=108, num_lanes=2, direction=3),    # 10
    Road(x=0,   y=167,  grid_len=108, num_lanes=2, direction=4),    # 11

    Road(x=5,   y=168,  grid_len=92, num_lanes=1, direction=1),     # 12
    Road(x=97,  y=170,  grid_len=92, num_lanes=1, direction=2),     # 13

    Road(x=102, y=171,  grid_len=52, num_lanes=2, direction=3),     # 14
    Road(x=98,  y=223,  grid_len=52, num_lanes=2, direction=4),     # 15

    Road(x=4,   y=171,  grid_len=52, num_lanes=2, direction=3),     # 16
    Road(x=0,   y=223,  grid_len=52, num_lanes=2, direction=4),     # 17

    Road(x=5,   y=224,  grid_len=92, num_lanes=1, direction=1),     # 18
    Road(x=97,  y=226,  grid_len=92, num_lanes=1, direction=2)      # 19

]

# edge roads are used for feeding simulation area with cars and as a sink for cars leaving the area
edge_roads = [
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 0
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=2),  # 1

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=3),  # 2
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=4),  # 3

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=3),  # 4
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=4),  # 5

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 6
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 7

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 8
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 9

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 10
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 11

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 12
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 13

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 14
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 15

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 16
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 17

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 18
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 19

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 20
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 21

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 22
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 23
]

num_cars_to_generate = [
    [4, 3, 3, 4, 3],        # 0
    [8, 4, 2, 12, 2],       # 1
    [5, 7, 4, 4, 3],        # 2
    [3, 8, 10, 5, 7],       # 3
    [5, 5, 3, 1, 9],        # 4
    [3, 2, 3, 4, 3],        # 5
    [6, 1, 9, 10, 3],       # 6
    [2, 13, 11, 10, 7],     # 7
    [2, 12, 6, 2, 9],       # 8
    [0, 0, 0, 0, 0],        # 9
    [9, 8, 6, 7, 8],        # 10
    [0, 0, 0, 0, 0]         # 11
]

edges = [
    Edge(in_road=edge_roads[0], out_road=edge_roads[1], num_cars_to_generate=num_cars_to_generate[0][0]),       # 0
    Edge(in_road=edge_roads[2], out_road=edge_roads[3], num_cars_to_generate=num_cars_to_generate[1][0]),       # 1
    Edge(in_road=edge_roads[4], out_road=edge_roads[5], num_cars_to_generate=num_cars_to_generate[2][0]),       # 2
    Edge(in_road=edge_roads[6], out_road=edge_roads[7], num_cars_to_generate=num_cars_to_generate[3][0]),       # 3
    Edge(in_road=edge_roads[8], out_road=edge_roads[9], num_cars_to_generate=num_cars_to_generate[4][0]),       # 4
    Edge(in_road=edge_roads[10], out_road=edge_roads[11], num_cars_to_generate=num_cars_to_generate[5][0]),     # 5
    Edge(in_road=edge_roads[12], out_road=edge_roads[13], num_cars_to_generate=num_cars_to_generate[6][0]),     # 6
    Edge(in_road=edge_roads[14], out_road=edge_roads[15], num_cars_to_generate=num_cars_to_generate[7][0]),     # 7
    Edge(in_road=edge_roads[16], out_road=edge_roads[17], num_cars_to_generate=num_cars_to_generate[8][0]),     # 8
    Edge(in_road=edge_roads[18], out_road=edge_roads[19], num_cars_to_generate=num_cars_to_generate[9][0]),     # 9
    Edge(in_road=edge_roads[20], out_road=edge_roads[21], num_cars_to_generate=num_cars_to_generate[10][0]),    # 10
    Edge(in_road=edge_roads[22], out_road=edge_roads[23], num_cars_to_generate=num_cars_to_generate[11][0])     # 11
]

intersections = [
    Intersection(in_roads=[edge_roads[2], roads[1], roads[5], edge_roads[0]],                       # 0
                 out_roads=[edge_roads[3], roads[0], roads[4], edge_roads[1]],
                 neighbourhood=[
                     [[[3, 0, 0.6], [2, 0, 0], [1, 0, 0.4]]],
                     [[[0, 0, 0.5], [3, 0, 0.5], [2, 0, 0]]],
                     [[[1, 0, 0], [0, 0, 0.5], [3, 0, 0.5]], [[1, 0, 0.7], [0, 0, 0.3], [3, 0, 0]]],
                     [[[2, 1, 0], [1, 0, 0.5], [0, 0, 0.5]]]  # done
                 ]),
    Intersection(in_roads=[edge_roads[4], edge_roads[6], roads[3], roads[0]],                       # 1
                 out_roads=[edge_roads[5], edge_roads[7], roads[2], roads[1]],
                 neighbourhood=[
                     [[[3, 0, 0.7], [2, 0, 0.3], [1, 0, 0]]],
                     [[[0, 0, 0], [3, 0, 0.6], [2, 0, 0.4]]],
                     [[[1, 0, 0], [0, 0, 0.3], [3, 0, 0.7]], [[1, 0, 0.5], [0, 0, 0.5], [3, 0, 0]]],
                     [[[2, 1, 0.6], [1, 0, 0.2], [0, 0, 0.2]]]  # done
                 ]),
    Intersection(in_roads=[roads[4], roads[7], roads[11], edge_roads[22]],                          # 2
                 out_roads=[roads[5], roads[6], roads[10], edge_roads[23]],
                 neighbourhood=[
                     [[[3, 0, 0], [2, 0, 0.7], [1, 0, 0.3]], [[3, 0, 0.3], [2, 1, 0.7], [1, 0, 0]]],
                     [[[0, 1, 0.4], [3, 0, 0.2], [2, 0, 0.4]]],
                     [[[1, 0, 0], [0, 0, 0.7], [3, 0, 0.3]], [[1, 0, 0.3], [0, 1, 0.7], [3, 0, 0]]],
                     [[[2, 1, 0.4], [1, 0, 0.4], [0, 0, 0.2]]]  # done
                 ]),
    Intersection(in_roads=[roads[2], edge_roads[8], roads[9], roads[6]],                            # 3
                 out_roads=[roads[3], edge_roads[9], roads[8], roads[7]],
                 neighbourhood=[
                     [[[3, 0, 0], [2, 0, 0.7], [1, 0, 0.3]], [[3, 0, 0], [2, 1, 1], [1, 0, 0]]],
                     [[[0, 1, 0.4], [3, 0, 0], [2, 0, 0.6]]],
                     [[[1, 0, 0], [0, 0, 1], [3, 0, 0]], [[1, 0, 0.5], [0, 1, 0.5], [3, 0, 0]]],
                     [[[2, 1, 0.4], [1, 0, 0.2], [0, 0, 0.4]]]  # done
                 ]),
    Intersection(in_roads=[roads[10], roads[13], roads[17], edge_roads[20]],                        # 4
                 out_roads=[roads[11], roads[12], roads[16], edge_roads[21]],
                 neighbourhood=[
                     [[[3, 0, 0], [2, 0, 0.7], [1, 0, 0.3]], [[3, 0, 0.3], [2, 1, 0.7], [1, 0, 0]]],
                     [[[0, 1, 0], [3, 0, 0.5], [2, 0, 0.5]]],
                     [[[1, 0, 0], [0, 0, 0], [3, 0, 1]], [[1, 0, 1], [0, 1, 0], [3, 0, 0]]],
                     [[[2, 1, 0.4], [1, 0, 0.6], [0, 0, 0]]]  # done
                 ]),
    Intersection(in_roads=[roads[8], edge_roads[10], roads[15], roads[12]],                         # 5
                 out_roads=[roads[9], edge_roads[11], roads[14], roads[13]],
                 neighbourhood=[
                     [[[3, 0, 0], [2, 0, 0.7], [1, 0, 0.3]], [[3, 0, 0.4], [2, 1, 0.6], [1, 0, 0]]],
                     [[[0, 1, 0.1], [3, 0, 0.5], [2, 0, 0.4]]],
                     [[[1, 0, 0], [0, 0, 0.4], [3, 0, 0.6]], [[1, 0, 0.5], [0, 1, 0.5], [3, 0, 0]]],
                     [[[2, 1, 0.4], [1, 0, 0.2], [0, 0, 0.4]]]  # done
                 ]),
    Intersection(in_roads=[roads[16], roads[19], edge_roads[16], edge_roads[18]],                   # 6
                 out_roads=[roads[17], roads[18], edge_roads[17], edge_roads[19]],
                 neighbourhood=[
                     [[[3, 0, 0], [2, 0, 1], [1, 0, 0]], [[3, 0, 0.5], [2, 0, 0.5], [1, 0, 0]]],
                     [[[0, 1, 0.7], [3, 0, 0.2], [2, 0, 0.1]]],
                     [[[1, 0, 0], [0, 0, 1], [3, 0, 0]]],
                     [[[2, 0, 0], [1, 0, 0], [0, 0, 1]]]  # done
                 ]),
    Intersection(in_roads=[roads[14], edge_roads[12], edge_roads[14], roads[18]],                   # 7
                 out_roads=[roads[15], edge_roads[13], edge_roads[15], roads[19]],
                 neighbourhood=[
                     [[[3, 0, 0], [2, 0, 0.5], [1, 0, 0.5]], [[3, 0, 0.6], [2, 0, 0.4], [1, 0, 0]]],
                     [[[0, 1, 0.7], [3, 0, 0.3], [2, 0, 0]]],
                     [[[1, 0, 0], [0, 0, 0.8], [3, 0, 0.2]]],
                     [[[2, 0, 0], [1, 0, 0], [0, 0, 0]]]  # done
                 ])
]

edge_intersections = [
    Intersection(in_roads=[edge_roads[1]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[3]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[5]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[7]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[9]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[11]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[13]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[15]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[17]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[19]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[21]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[23]], out_roads=[], neighbourhood=[])
]

# for test purposes
# self.intersections[1].allowed_neighbourhood[0][0] = []

# roads[0].add_car(Car(uuid.uuid4(), car_type=4, x=24, lane=0))
# roads[0].add_car(Car(uuid.uuid4(), car_type=1, x=16, lane=0))
# roads[0].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
#
# roads[1].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[1].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[1].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[2].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[2].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[2].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[3].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[3].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[3].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[4].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[4].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[4].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[5].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[5].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[5].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[6].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[6].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[6].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[7].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[7].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[7].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[8].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[8].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[8].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[9].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[9].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[9].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[10].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[10].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[10].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[11].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[11].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[11].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[12].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[12].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[12].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[13].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[13].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[13].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[14].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[14].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[14].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[15].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[15].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[15].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[16].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[16].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[16].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[17].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[17].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[17].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[18].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[18].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[18].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[19].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[19].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[19].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))

# self.roads[1].add_car(Car(-1, car_type=1, x=10, lane=0))
