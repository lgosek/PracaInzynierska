car_types = (
    # [length, v_max, color]
    (1, 7, 'c'),  # motorcycle
    (2, 6, 'r'),  # passenger car
    (3, 5, 'g'),  # van
    # [4, 4, 'b'],  # minibus
    (5, 4, 'm'),  # bus/truck
    (6, 4, 'k')  # lorry
)

MAX_VEL = 6


class Car:
    def __init__(self, id_num, car_type, x, lane):
        """
        Class representing individual car
        :param id_num: cars unique id
        :param car_type: type of the car, according to car types tuple
        :param x: lengthwise position on the road of car's head
        :param lane: number of lane of subsequent segments of the car
        """
        self.id_num = id_num
        self.length = car_types[car_type][0]
        self.v_max = car_types[car_type][1]
        self.v_temp = 0
        self.pos_x = list(range(x + 1 - self.length, x + 1))
        self.pos_y = [lane for _ in range(self.length)]
        self.color = car_types[car_type][2]

        self.on_two_roads = False  # flag telling if car is during a road switch
        self.changing_road = False  # if car is about to change road on an intersection
        self.chosen_road_and_lane = None  # index of a road and lane chosen on an intersection
        self.turning_right = False  # flag if the car is turning to the right on an intersection.
        # Used for collision avoiding
        self.turning_left = False

        self.moved = False  # flag telling if the car already moved in this iteration

        self.old_grid_len = 0  # length of the previous road the car was on before road change.
        # Used for correct position update

    def head(self):
        """
        Function returns position of the first segment of the vehicle
        :return: position of the first segment of the vehicle
        """
        return self.pos_x[-1]

    def tail(self):
        """
        Function returns position of the last segment of the vehicle
        :return: position of the last segment of the vehicle
        """
        return self.pos_x[0]
