from intersections import Intersection
from roads import Road
from edges import Edge

# array of road models - visible part of simulated roads
roads = [
    Road(x=5,   y=0,    grid_len=84, num_lanes=1, direction=0),     # 0
    Road(x=88,  y=1,    grid_len=84, num_lanes=2, direction=180),     # 1

    Road(x=94,  y=0,    grid_len=136, num_lanes=1, direction=0),     # 2
    Road(x=229, y=1,    grid_len=136, num_lanes=2, direction=180),     # 3

    Road(x=235, y=4,    grid_len=140, num_lanes=1, direction=90),     # 4
    Road(x=231, y=141,   grid_len=140, num_lanes=2, direction=270),     # 5

    Road(x=229, y=144,   grid_len=136, num_lanes=1, direction=180),     # 6
    Road(x=94,  y=142,   grid_len=136, num_lanes=1, direction=0),     # 7

    Road(x=88,  y=144,   grid_len=60, num_lanes=1, direction=180),    # 8 FOR PLOT CHANGE LEN TO 84 (real 60)
    Road(x=29,  y=142,   grid_len=60, num_lanes=1, direction=0),    # 9 FOR PLOT CHANGE LEN TO 84

    Road(x=25,   y=141,   grid_len=148, num_lanes=2, direction=270, segments=[(32, 270), (39, 232.02), (77, 270)]),    # 10
    Road(x=4,   y=2,    grid_len=148, num_lanes=2, direction=90, segments=[(77, 90), (39, 52.02), (32, 90)]),    # 11

    Road(x=90,   y=141,  grid_len=140, num_lanes=2, direction=270),     # 12
    Road(x=93,   y=2,    grid_len=140, num_lanes=2, direction=90)      # 13
]

# edge roads are used for feeding simulation area with cars and as a sink for cars leaving the area
edge_roads = [
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 0
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=2),  # 1

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=3),  # 2
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=4),  # 3

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=3),  # 4
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=4),  # 5

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 6
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 7

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 8
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 9

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 10
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 11

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 12
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 13

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 14
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 15

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 16
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 17

    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1),  # 18
    Road(x=0, y=0, grid_len=100, num_lanes=1, direction=1)   # 19
]

num_cars_to_generate = [
    [4, 3, 3, 4, 3],        # 0
    [8, 4, 2, 12, 2],       # 1
    [5, 7, 4, 4, 3],        # 2
    [3, 8, 10, 5, 7],       # 3
    [5, 5, 3, 1, 9],        # 4
    [3, 2, 3, 4, 3],        # 5
    [6, 1, 9, 10, 3],       # 6
    [2, 13, 11, 10, 7],     # 7
    [2, 12, 6, 2, 9],       # 8
    [0, 0, 0, 0, 0]         # 9
]

edges = [
    Edge(in_road=edge_roads[0], out_road=edge_roads[1], num_cars_to_generate=num_cars_to_generate[0][0]),       # 0
    Edge(in_road=edge_roads[2], out_road=edge_roads[3], num_cars_to_generate=num_cars_to_generate[1][0]),       # 1
    Edge(in_road=edge_roads[4], out_road=edge_roads[5], num_cars_to_generate=num_cars_to_generate[2][0]),       # 2
    Edge(in_road=edge_roads[6], out_road=edge_roads[7], num_cars_to_generate=num_cars_to_generate[3][0]),       # 3
    Edge(in_road=edge_roads[8], out_road=edge_roads[9], num_cars_to_generate=num_cars_to_generate[4][0]),       # 4
    Edge(in_road=edge_roads[10], out_road=edge_roads[11], num_cars_to_generate=num_cars_to_generate[5][0]),     # 5
    Edge(in_road=edge_roads[12], out_road=edge_roads[13], num_cars_to_generate=num_cars_to_generate[6][0]),     # 6
    Edge(in_road=edge_roads[14], out_road=edge_roads[15], num_cars_to_generate=num_cars_to_generate[7][0]),     # 7
    Edge(in_road=edge_roads[16], out_road=edge_roads[17], num_cars_to_generate=num_cars_to_generate[8][0]),     # 8
    Edge(in_road=edge_roads[18], out_road=edge_roads[19], num_cars_to_generate=num_cars_to_generate[9][0])      # 9
]

intersections = [
    Intersection(in_roads=[edge_roads[2], roads[1], roads[10], edge_roads[18]],                       # 0
                 out_roads=[edge_roads[1], roads[0], roads[11], edge_roads[19]],
                 neighbourhood=[
                     [[[3, 0, 0.6], [2, 0, 0], [1, 0, 0.4]]],
                     [[[0, 0, 0.5], [3, 0, 0.5], [2, 0, 0]], [[0, 0, 0.5], [3, 0, 0.5], [2, 0, 0]]],
                     [[[1, 0, 0], [0, 0, 0.5], [3, 0, 0.5]], [[1, 0, 0.7], [0, 0, 0.3], [3, 0, 0]]],
                     [[[2, 1, 0], [1, 0, 0.5], [0, 0, 0.5]]]  # done
                 ]),
    Intersection(in_roads=[edge_roads[2], roads[3], roads[12], roads[0]],                       # 1
                 out_roads=[edge_roads[3], roads[2], roads[13], roads[1]],
                 neighbourhood=[
                     [[[3, 0, 0.7], [2, 0, 0.3], [1, 0, 0]]],
                     [[[0, 0, 0], [3, 0, 0.6], [2, 0, 0.4]], [[0, 0, 0], [3, 0, 0.6], [2, 0, 0.4]]],
                     [[[1, 0, 0], [0, 0, 0.3], [3, 0, 0.7]], [[1, 0, 0.5], [0, 0, 0.5], [3, 0, 0]]],
                     [[[2, 1, 0.6], [1, 0, 0.2], [0, 0, 0.2]]]  # done
                 ]),
    Intersection(in_roads=[edge_roads[4], edge_roads[6], roads[5], roads[2]],                          # 2
                 out_roads=[edge_roads[5], edge_roads[7], roads[4], roads[3]],
                 neighbourhood=[
                     [[[3, 0, 0], [2, 0, 0.7], [1, 0, 0.3]]],
                     [[[0, 1, 0.4], [3, 0, 0.2], [2, 0, 0.4]]],
                     [[[1, 0, 0], [0, 0, 0.7], [3, 0, 0.3]], [[1, 0, 0.3], [0, 1, 0.7], [3, 0, 0]]],
                     [[[2, 1, 0.4], [1, 0, 0.4], [0, 0, 0.2]]]  # done
                 ]),
    Intersection(in_roads=[roads[11], roads[8], edge_roads[14], edge_roads[16]],                            # 3
                 out_roads=[roads[10], roads[9], edge_roads[15], edge_roads[17]],
                 neighbourhood=[
                     [[[3, 0, 0], [2, 0, 0.7], [1, 0, 0.3]], [[3, 0, 0], [2, 1, 1], [1, 0, 0]]],
                     [[[0, 1, 0.4], [3, 0, 0], [2, 0, 0.6]]],
                     [[[1, 0, 0], [0, 0, 1], [3, 0, 0]]],
                     [[[2, 1, 0.4], [1, 0, 0.2], [0, 0, 0.4]]]  # done
                 ]),
    Intersection(in_roads=[roads[13], roads[6], edge_roads[12], roads[9]],                        # 4
                 out_roads=[roads[12], roads[7], edge_roads[13], roads[8]],
                 neighbourhood=[
                     [[[3, 0, 0], [2, 0, 0.7], [1, 0, 0.3]], [[3, 0, 0.3], [2, 1, 0.7], [1, 0, 0]]],
                     [[[0, 1, 0], [3, 0, 0.5], [2, 0, 0.5]]],
                     [[[1, 0, 0], [0, 0, 0], [3, 0, 1]]],
                     [[[2, 1, 0.4], [1, 0, 0.6], [0, 0, 0]]]  # done
                 ]),
    Intersection(in_roads=[roads[4], edge_roads[8], edge_roads[10], roads[7]],                         # 5
                 out_roads=[roads[5], edge_roads[9], edge_roads[11], roads[6]],
                 neighbourhood=[
                     [[[3, 0, 0], [2, 0, 0.7], [1, 0, 0.3]]],
                     [[[0, 1, 0.1], [3, 0, 0.5], [2, 0, 0.4]]],
                     [[[1, 0, 0], [0, 0, 0.4], [3, 0, 0.6]]],
                     [[[2, 1, 0.4], [1, 0, 0.2], [0, 0, 0.4]]]  # done
                 ])
]

edge_intersections = [
    Intersection(in_roads=[edge_roads[1]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[3]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[5]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[7]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[9]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[11]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[13]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[15]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[17]], out_roads=[], neighbourhood=[]),
    Intersection(in_roads=[edge_roads[19]], out_roads=[], neighbourhood=[])
]

# for test purposes
# self.intersections[1].allowed_neighbourhood[0][0] = []

# roads[0].add_car(Car(uuid.uuid4(), car_type=4, x=24, lane=0))
# roads[0].add_car(Car(uuid.uuid4(), car_type=1, x=16, lane=0))
# roads[0].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
#
# roads[1].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[1].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[1].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[2].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[2].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[2].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[3].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[3].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[3].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[4].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[4].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[4].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[5].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[5].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[5].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[6].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[6].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[6].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[7].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[7].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[7].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[8].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[8].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[8].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[9].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[9].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[9].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[10].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[10].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[10].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[11].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[11].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[11].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[12].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[12].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[12].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[13].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[13].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[13].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[14].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[14].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[14].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[15].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[15].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[15].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[16].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[16].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[16].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[17].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[17].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[17].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[18].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[18].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[18].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))
#
# roads[19].add_car(Car(uuid.uuid4(), car_type=4, x=20, lane=0))
# roads[19].add_car(Car(uuid.uuid4(), car_type=0, x=13, lane=0))
# roads[19].add_car(Car(uuid.uuid4(), car_type=1, x=10, lane=0))

# self.roads[1].add_car(Car(-1, car_type=1, x=10, lane=0))
