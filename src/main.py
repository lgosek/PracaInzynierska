import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from simulation import Simulation
from utils import TRAIN_SET_MINUTES

if __name__ == '__main__':
    sim = Simulation(show_plot=False, simulation_time=(120 - TRAIN_SET_MINUTES)*60, prep_data=False)
    outcome = sim.start()

    df = pd.read_csv("../logs/diff.csv", sep=',', header=None, index_col=None)
    print(df)
    df = df.abs()
    ar = df.to_numpy()
    ar[ar == np.inf] = 1
    # print(np.any(np.isnan(ar)))
    ar[np.isnan(ar)] = 0
    # print(ar)
    means = np.mean(ar, axis=1)
    print(means)
    df = df.transpose()
    # df.replace(to_replace=np.inf, value=0, inplace=True)
    # print(df)

    # plt.figure()
    df.plot()
    # plt.ylim((0, 7))
    plt.xlabel("Minuta symulacji")
    plt.ylabel("Błąd względny")
    plt.legend(["A104", "A23", "A28", "A46", "A12", "A59"])
    plt.grid(axis='y')
    plt.show()
