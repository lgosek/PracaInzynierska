import random
from typing import List

from cars import Car
from roads import Road, p
from copy import deepcopy
import numpy as np


SENSOR_POS = 4


class Intersection:
    def __init__(self, in_roads: List[Road], out_roads: List[Road],
                 neighbourhood):
        """
        Class representing an intersection
        :param in_roads: list of roads incoming to the intersection in order S, E, N, W
        :param out_roads: list of roads outgoing from an intersection
        :param neighbourhood: neighbourhood of lanes: for each incoming road, for each lane we specify a list of
                            of neighbouring lanes - tuple (out_road_index, lane_number, probability) in order
                            "left, front, right"
        """
        self.in_roads = in_roads
        self.out_roads = out_roads
        self.allowed_neighbourhood = neighbourhood

        # initializing with empty neighbourhoods
        self.neighbourhood = [[[] for _ in range(road.num_lanes)] for road in self.in_roads]
        # self.neighbourhood = neighbourhood
        # initializing with all red lights on the intersection
        self.green_light = None

        self.vehicle_passed_counter = 0

    def update_probabilities(self, probabilities):
        for in_road_num, in_road in enumerate(self.allowed_neighbourhood):
            for lane_num, lane in enumerate(in_road):
                if len(in_road) == 1:
                    for j in range(3):
                        lane[j][2] = probabilities[in_road_num][j]
                else:
                    if lane_num == 0:
                        lane[0][2] = 0
                        lane[1][2] = probabilities[in_road_num][1]
                        lane[2][2] = probabilities[in_road_num][2]
                    elif lane_num == len(in_road) - 1:
                        lane[0][2] = probabilities[in_road_num][0]
                        lane[1][2] = probabilities[in_road_num][1]
                        lane[2][2] = 0
                    else:
                        lane[0][2] = 0
                        lane[1][2] = probabilities[in_road_num][1]
                        lane[2][2] = 0

    @staticmethod
    def _opposite_road_index(road_idx):
        return (road_idx + 2) % 4

    def update_car_velocities(self):
        """
        Method performs velocity update on each car for each road incoming to the intersection
        """
        for road_idx, road in enumerate(self.in_roads):
            for car in road.traffic:
                if car.id_num == -1:
                    continue

                # determining on which lane the car currently is
                current_lane = car.pos_y[-1]

                # checking if in this iteration the car will stay on this road or leave it
                if car.head() + car.v_temp < road.grid_len - 1:
                    # finding cars whose last segment is on the same lane
                    cars_with_end_on_the_same_lane = [c for c in road.traffic if c.pos_y[0] == car.pos_y[-1]]\
                                                     + [c for c in road.leaving_traffic if c.pos_y[0] == car.pos_y[-1]]

                    allowed_area_len = road.grid_len
                else:
                    # road choice and combine traffic from two roads
                    try:
                        neighbouring_lanes = [x[:2] for x in self.neighbourhood[road_idx][current_lane]]
                        probabilities = [x[-1] for x in self.neighbourhood[road_idx][current_lane]]
                    except IndexError:
                        car.chosen_road_and_lane = None
                        car.v_temp = 0
                        continue

                    if len(neighbouring_lanes) > 0:
                        car.changing_road = True

                        if car.chosen_road_and_lane == (-1, -1):
                            # print("elo")
                            try:
                                chosen_road_and_lane = self.neighbourhood[road_idx][-1][0][:2]
                            except IndexError:
                                chosen_road_and_lane = car.chosen_road_and_lane
                            # print(chosen_road_and_lane)
                            if chosen_road_and_lane == (-1, -1):
                                car.v_temp = 0
                                continue
                        elif car.chosen_road_and_lane == (-2, -2):
                            # print("right elo")
                            try:
                                chosen_road_and_lane = self.neighbourhood[road_idx][0][-1][:2]
                            except IndexError:
                                chosen_road_and_lane = car.chosen_road_and_lane
                            # print(chosen_road_and_lane)
                            if chosen_road_and_lane == (-2, -2):
                                car.v_temp = 0
                                continue
                        else:
                            chosen_road_and_lane = random.choices(neighbouring_lanes, probabilities)[0]
                            # print(f"Car {car.id_num}, road {chosen_road_and_lane}")

                            if chosen_road_and_lane == (-1, -1) or chosen_road_and_lane == (-2, -2):
                                car.chosen_road_and_lane = chosen_road_and_lane
                                car.v_temp = 0
                                continue

                        # ensuring yielding on intersections
                        # checking if car is turning right
                        if chosen_road_and_lane[0] == (road_idx + 1) % 4:
                            # print("right")
                            car.turning_right = True
                            try:
                                left_neigh_tuple = self.neighbourhood[self._opposite_road_index(road_idx)][-1][0]
                                # blocking left turn neighbourhood on the opposite road
                                self.neighbourhood[self._opposite_road_index(road_idx)][-1][0] = \
                                    (-1, -1, left_neigh_tuple[2])
                            except IndexError:
                                pass
                        # checking if car is turning left
                        elif chosen_road_and_lane[0] == (road_idx - 1) % 4:
                            # print("left")
                            car.turning_left = True
                            try:
                                right_neigh_tuple = self.neighbourhood[self._opposite_road_index(road_idx)][0][-1]
                                # blocking right turn neighbourhood on the opposite road
                                self.neighbourhood[self._opposite_road_index(road_idx)][0][-1] = \
                                    (-2, -2, right_neigh_tuple[2])
                            except IndexError:
                                pass

                        # combining traffic
                        dest_traffic_raw_copy = deepcopy(self.out_roads[chosen_road_and_lane[0]].traffic)

                        # fixing lane numbers while combining traffic from two roads
                        dest_traffic_copy = [c for c in dest_traffic_raw_copy if chosen_road_and_lane[1] in c.pos_y]
                        for c in dest_traffic_copy:
                            y_target = np.where(np.array(c.pos_y) == chosen_road_and_lane[1])
                            c.pos_x = (np.array(c.pos_x)[y_target]).tolist()
                            c.pos_y = (np.array(c.pos_y)[y_target]).tolist()
                            # if len(car.pos)
                            c.pos_x = [x + road.grid_len for x in c.pos_x]
                            c.pos_y = [current_lane for _ in c.pos_y]

                        combined_traffic = road.traffic + road.leaving_traffic + dest_traffic_copy

                        # finding cars whose last segment is on the same lane
                        cars_with_end_on_the_same_lane = [c for c in combined_traffic if c.pos_y[0] == car.pos_y[-1]]

                        # saving info that car changes road and the road and lane chosen
                        car.changing_road = True
                        car.chosen_road_and_lane = chosen_road_and_lane
                        car.old_grid_len = road.grid_len

                        allowed_area_len = road.grid_len + self.out_roads[chosen_road_and_lane[0]].grid_len
                    else:
                        cars_with_end_on_the_same_lane = [c for c in road.traffic if c.pos_y[0] == car.pos_y[-1]] \
                                                         + [c for c in road.leaving_traffic if
                                                            c.pos_y[0] == car.pos_y[-1]]
                        allowed_area_len = road.grid_len

                # finding the nearest car on current lane and distance to it
                pred, dist = Road.find_nearest_car(car, cars_with_end_on_the_same_lane, front=True)

                # calculating parameters and updating velocity
                if pred is None or car.id_num == pred.id_num:
                    # print("*")
                    dist = allowed_area_len - car.head() - 1  # car.v_max + 1
                else:
                    dist = (pred.tail() - car.head()) % road.grid_len - 1
                v_temp = min(car.v_temp + 1, dist, car.v_max)
                car.v_temp = max(v_temp - 1, 0) if random.random() < p else v_temp

                if car.head() + car.v_temp < road.grid_len:
                    car.changing_road = False
                    car.chosen_road_and_lane = None

    def update_car_positions(self):
        """
        Method performs position update on each car for each road incoming to the intersection
        """
        for road_idx, road in enumerate(self.in_roads):
            for car in reversed(road.traffic):
                if car.id_num == -1:
                    continue
                # handling obsolete updates on cars that switched roads
                if car.moved:
                    continue

                old_head = car.head()
                # moving the car
                if car.on_two_roads:
                    car.pos_x = [(x + car.v_temp) % car.old_grid_len for x in car.pos_x]
                else:
                    car.pos_x = [(x + car.v_temp) % road.grid_len for x in car.pos_x]
                car.moved = True

                if old_head <= road.grid_len - SENSOR_POS and car.head() > road.grid_len  - SENSOR_POS:
                    self.vehicle_passed_counter += 1

                # handling lane changes of non-head segments of the vehicle
                y_change_len = min(car.v_temp, car.length)
                if y_change_len > 0:
                    road_idx = len(car.pos_y) - 1
                    len_newlane = 1
                    while road_idx > 0:
                        if car.pos_y[road_idx - 1] == car.pos_y[road_idx]:
                            len_newlane += 1
                        else:
                            break
                        road_idx -= 1

                    if len_newlane == 1:
                        len_newlane = 0

                    car.pos_y[-(len_newlane + car.v_temp):] = \
                        [car.pos_y[-1] for _ in range(min(len_newlane + car.v_temp, car.length))]

                # transitioning the car into new road if it changes it
                if car.changing_road:
                    # adding the car to the new road
                    self.out_roads[car.chosen_road_and_lane[0]].add_car(car)

                    # finding the place in the car where split between old and new road occurs
                    try:
                        pos_new_road = car.pos_x.index(0)
                        if pos_new_road > 0:
                            car.on_two_roads = True
                    except ValueError:
                        pos_new_road = 0

                    # assigning new lane number for part of the car that is on a new road
                    car.pos_y[pos_new_road:] = [
                        car.chosen_road_and_lane[1] for _ in range(len(car.pos_y) - pos_new_road)
                    ]

                    # removing the car from old road
                    road.remove_car(car)

                    # if the car didn't leave old road completely we mark it as a "car leaving the road"
                    if pos_new_road > 0:
                        road.add_leaving_car(car)
                    else:
                        # self.vehicle_passed_counter += 1

                        if car.turning_right:
                            try:
                                self.neighbourhood[self._opposite_road_index(road_idx)][-1][0] = \
                                    deepcopy(self.allowed_neighbourhood[self._opposite_road_index(road_idx)][-1][0])
                            except IndexError:
                                pass
                            car.turning_right = False
                        elif car.turning_left:
                            try:
                                self.neighbourhood[self._opposite_road_index(road_idx)][0][-1] = \
                                    deepcopy(self.allowed_neighbourhood[self._opposite_road_index(road_idx)][0][-1])
                            except IndexError:
                                pass
                            car.turning_left = False

                    # resetting road change flag
                    car.changing_road = False

        # deleting cars that actually left the road
        for road_idx, road in enumerate(self.in_roads):
            for car in road.leaving_traffic:
                if sorted(car.pos_x) == car.pos_x:
                    road.remove_leaving_car(car)
                    car.on_two_roads = False

                    # self.vehicle_passed_counter += 1

                    if car.turning_right:
                        try:
                            self.neighbourhood[self._opposite_road_index(road_idx)][-1][0] = \
                                deepcopy(self.allowed_neighbourhood[self._opposite_road_index(road_idx)][-1][0])
                        except IndexError:
                            pass
                        car.turning_right = False
                    elif car.turning_left:
                        try:
                            self.neighbourhood[self._opposite_road_index(road_idx)][0][-1] = \
                                deepcopy(self.allowed_neighbourhood[self._opposite_road_index(road_idx)][0][-1])
                        except IndexError:
                            pass
                        car.turning_left = False

    def perform_lane_changes_intersection_level(self):
        """
        Method performs lane changes on each road incoming to the intersection
        """
        for road in self.in_roads:
            road.perform_lane_changes()

    @staticmethod
    def _add_red_light(road):
        for lane in range(road.num_lanes):
            road.add_car(Car(-1, 0, road.grid_len, lane))

    @staticmethod
    def _remove_red_light(road):
        for car in reversed(road.traffic):
            if car.id_num == -1:
                road.traffic.remove(car)

    def perform_traffic_light_switch(self):
        """
        Method switching traffic lights on the intersection
        """
        if self.green_light == 'EW' or self.green_light is None:
            self.green_light = 'NS'
            self.neighbourhood[0] = deepcopy(self.allowed_neighbourhood[0])
            self.neighbourhood[2] = deepcopy(self.allowed_neighbourhood[2])
            self._remove_red_light(self.in_roads[0])
            self._remove_red_light(self.in_roads[2])
            self.neighbourhood[1] = [[] for _ in range(self.in_roads[1].num_lanes)]
            self.neighbourhood[3] = [[] for _ in range(self.in_roads[3].num_lanes)]
            # if self.green_light is not None:
            self._add_red_light(self.in_roads[1])
            self._add_red_light(self.in_roads[3])
        else:
            self.green_light = 'EW'
            self.neighbourhood[1] = deepcopy(self.allowed_neighbourhood[1])
            self.neighbourhood[3] = deepcopy(self.allowed_neighbourhood[3])
            self._remove_red_light(self.in_roads[1])
            self._remove_red_light(self.in_roads[3])
            self.neighbourhood[0] = [[] for _ in range(self.in_roads[0].num_lanes)]
            self.neighbourhood[2] = [[] for _ in range(self.in_roads[2].num_lanes)]
            self._add_red_light(self.in_roads[0])
            self._add_red_light(self.in_roads[2])

    def switch_all_red_lights(self):
        self.neighbourhood = [[[] for _ in range(road.num_lanes)] for road in self.in_roads]
        if self.green_light is None:
            self._add_red_light(self.in_roads[0])
            self._add_red_light(self.in_roads[1])
            self._add_red_light(self.in_roads[2])
            self._add_red_light(self.in_roads[3])
        elif self.green_light == 'EW':
            self._add_red_light(self.in_roads[1])
            self._add_red_light(self.in_roads[3])
        elif self.green_light == 'NS':
            self._add_red_light(self.in_roads[0])
            self._add_red_light(self.in_roads[2])

