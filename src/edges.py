import uuid

from cars import Car
from roads import Road
from random import random, choices


class Edge:
    def __init__(self, in_road: Road, out_road: Road, num_cars_to_generate):
        """
        Class representing Edge model
        :param in_road: reference to the road incoming to the simulation area (generator)
        :param out_road: reference to the road outgoing from the simulation (sink)
        :param num_cars_to_generate: number of cars to be generated in this minute of simulation
        """
        self.in_road = in_road
        self.out_road = out_road
        self.num_cars_to_generate = num_cars_to_generate

        self.time_left = 60
        self.num_generated = 0

        self._car_types_probabilities = [0.05, 0.6, 0.25, 0.05, 0.05]

    def _find_first_available_place(self):
        """
        Method finding a place closest to the simulation area for the new car to be put
        :return: index on the in_road
        """
        try:
            traffic_last = self.in_road.traffic[-1].tail() - 1
        except IndexError:
            traffic_last = self.in_road.grid_len - 1

        try:
            leaving_traffic_last = self.in_road.leaving_traffic[-1].tail() - 1
        except IndexError:
            leaving_traffic_last = self.in_road.grid_len - 1

        return min(traffic_last, leaving_traffic_last)

    def generate_cars(self):
        """
        Method generating appropriate number of cars in one iteration
        """
        i = 0
        while self.num_generated < self.num_cars_to_generate and i < self.num_cars_to_generate // 60 + 1:
            if random() < self.num_cars_to_generate / 60:
                self._generate_single_car()
            i += 1

        self.time_left -= 1

        if self.time_left == 0:
            while self.num_generated < self.num_cars_to_generate:
                self._generate_single_car()

    def _generate_single_car(self):
        """
        Method generating one single car and placing it on the in_road
        """
        car_type = choices([0, 1, 2, 3, 4], self._car_types_probabilities)[0]
        car = Car(uuid.uuid4(), car_type, self._find_first_available_place(), 0)
        self.in_road.add_car(car)
        self.num_generated += 1

    def terminate_cars(self):
        """
        Method deleting cars that left the simulation area
        """
        for car in self.out_road.traffic:
            if sorted(car.pos_x) == car.pos_x:
                self.out_road.remove_car(car)
                del car

    def reset(self, new_num_cars_to_generate):
        """
        Method resetting parameters at the end of a minute of simulation
        :param new_num_cars_to_generate: number of cars to be generated in the next minute
        """
        self.time_left = 60
        self.num_generated = 0
        self.num_cars_to_generate = new_num_cars_to_generate
