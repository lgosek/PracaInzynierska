import os

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import warnings
# import matplotlib.patches as patches

from new_city_architecture import roads, intersections, edges, edge_intersections, edge_roads
from utils import index_list, intersection_name_decoder, TRAIN_SET_MINUTES
from data_scraper import prepare_data
from math import sin, cos


SHIFT = 0.6

NS_GREEN = 60
EW_GREEN = 40

# OUT_COUNTS_CONST = 1.7

warnings.simplefilter(action='ignore', category=FutureWarning)


class Simulation:
    def __init__(self, show_plot=True, simulation_time=300, prep_data=False):
        print("SIMULATION INIT")
        self.time = 0
        self.simulation_time = simulation_time
        self.lights_period_time = 30
        self.background = plt.imread("../resources/new_background_light.png")
        if show_plot:
            self.fig, self.ax = plt.subplots()
            self.fig.canvas.mpl_connect('close_event', self.handle_close)
            self.ani = animation.FuncAnimation(self.fig, self.simulation_loop, interval=200, blit=False, save_count=50)

        self.roads = roads
        self.edge_roads = edge_roads
        self.intersections = intersections
        self.edges = edges
        self.edge_intersections = edge_intersections
        self.num_cars_to_generate = [[] for _ in range(len(self.edges))]  # num_cars_to_generate

        self.show_plot = show_plot

        self.simulation_outcome = [[] for _ in range(len(self.intersections))]
        self.expected_outcome = [[] for _ in range(len(self.intersections))]

        if prep_data:
            print("Preparing traffic data...")
            prepare_data()
            print("Done")
        print("Calculating intersection parameters...")
        self.initialize_params()
        print("Done")
        print("INIT COMPLETED")

    @staticmethod
    def _calculate_probabilities(in_counts, out_counts):
        # S E N W
        sum_in = np.sum(in_counts)
        sum_out = np.sum(out_counts)
        in_div = np.divide(in_counts, sum_in)
        out_div = np.divide(out_counts, sum_out)

        a = [
            [in_div[0], 0, 0, in_div[1], -in_div[2], -in_div[2], 0, 0],
            [0, in_div[0], -in_div[1], -in_div[1], 0, 0, in_div[3], 0],
            [-in_div[0], -in_div[0], 0, 0, in_div[2], 0, 0, in_div[3]],
            [0, 0, in_div[1], 0, 0, in_div[2], -in_div[3], -in_div[3]]
        ]

        b = [
            out_div[3] - in_div[2],
            out_div[2] - in_div[1],
            out_div[1] - in_div[0],
            out_div[0] - in_div[3]
        ]

        x = np.linalg.lstsq(a, b)[0]

        x[x < 0] = 0

        ret_arr = [
            [x[0], x[1], 1 - x[0] - x[1]],
            [x[2], x[3], 1 - x[2] - x[3]],
            [x[4], x[5], 1 - x[4] - x[5]],
            [x[6], x[7], 1 - x[6] - x[7]]
        ]

        z = np.matmul(a, x)

        # print()

        return ret_arr

    def initialize_params(self):
        for idx, intersection in enumerate(self.intersections):
            i = pd.read_csv(
                "../intersection_data/" + intersection_name_decoder[idx][0],
                header=None).to_numpy()[:TRAIN_SET_MINUTES]
            s = pd.read_csv(
                "../intersection_data/" + intersection_name_decoder[idx][1],
                header=None).to_numpy()[:TRAIN_SET_MINUTES]
            e = pd.read_csv(
                "../intersection_data/" + intersection_name_decoder[idx][2],
                header=None).to_numpy()[:TRAIN_SET_MINUTES]
            n = pd.read_csv(
                "../intersection_data/" + intersection_name_decoder[idx][3],
                header=None).to_numpy()[:TRAIN_SET_MINUTES]
            w = pd.read_csv(
                "../intersection_data/" + intersection_name_decoder[idx][4],
                header=None).to_numpy()[:TRAIN_SET_MINUTES]

            s = [row[2] for row in s]
            e = [row[3] for row in e]
            n = [row[0] for row in n]
            w = [row[1] for row in w]

            # s = s / OUT_COUNTS_CONST
            # e = e / OUT_COUNTS_CONST
            # n = n / OUT_COUNTS_CONST
            # w = w / OUT_COUNTS_CONST

            sum_in = i.sum(axis=0)
            s_out = sum(s)
            e_out = sum(e)
            n_out = sum(n)
            w_out = sum(w)

            probabilities = self._calculate_probabilities(
                in_counts=sum_in,
                out_counts=[s_out, e_out, n_out, w_out]
            )

            intersection.update_probabilities(probabilities)

            self.intersections[0].allowed_neighbourhood[0][0][2][2] = 0
            self.intersections[0].allowed_neighbourhood[2][1][0][2] = 0
            self.intersections[0].allowed_neighbourhood[3][0][1][2] = 0

            self.intersections[1].allowed_neighbourhood[0][0][2][2] = 0
            self.intersections[1].allowed_neighbourhood[2][1][0][2] = 0

            self.intersections[2].allowed_neighbourhood[0][0][1][2] = 0
            self.intersections[2].allowed_neighbourhood[1][0][2][2] = 0

            # print("")

            # expected outcome for intersection
            i_test = pd.read_csv(
                "../intersection_data/" + intersection_name_decoder[idx][0],
                header=None).to_numpy()[TRAIN_SET_MINUTES:]

            outcome = list(i_test.sum(axis=1))
            self.expected_outcome[idx] = outcome

            # num cars to generate
            if idx == 0:
                self.num_cars_to_generate[0] = [minute[0] for minute in i_test]
                self.num_cars_to_generate[9] = [minute[3] for minute in i_test]
            elif idx == 1:
                self.num_cars_to_generate[1] = [minute[0] for minute in i_test]
            elif idx == 2:
                self.num_cars_to_generate[2] = [minute[0] for minute in i_test]
                self.num_cars_to_generate[3] = [minute[1] for minute in i_test]
            elif idx == 3:
                self.num_cars_to_generate[7] = [minute[2] for minute in i_test]
                self.num_cars_to_generate[8] = [minute[3] for minute in i_test]
            elif idx == 4:
                self.num_cars_to_generate[6] = [minute[2] for minute in i_test]
            elif idx == 5:
                self.num_cars_to_generate[4] = [minute[1] for minute in i_test]
                self.num_cars_to_generate[5] = [minute[2] for minute in i_test]

            # print("")
        # for i in range(len(self.num_cars_to_generate)):
        #     for j in range(len(self.num_cars_to_generate[0])):
        #         self.num_cars_to_generate[i][j] *= 2

    def set_params(self, params):
        for intersection_idx, intersection in enumerate(self.intersections):
            for in_road_idx, in_road in enumerate(intersection.in_roads):
                for lane in range(in_road.num_lanes):
                    for i, j in enumerate(index_list[intersection_idx][in_road_idx][lane]):
                        intersection.allowed_neighbourhood[in_road_idx][lane][j][-1] = \
                            params[intersection_idx][in_road_idx][lane][i]

    # figure closing handling
    def handle_close(self, _):
        # for i, intersection in enumerate(self.intersections):
        #     print(f"{i}: {intersection.vehicle_passed_counter}")
        dirname = os.path.dirname(__file__)
        filename = os.path.join(dirname, '../logs/output.csv')
        out_file = open(filename, "w")

        for i, intersection in enumerate(self.intersections):
            self.simulation_outcome[i].append(intersection.vehicle_passed_counter)

        outcome = pd.DataFrame(self.simulation_outcome, dtype='int')
        outcome.to_csv(os.path.join(dirname, '../logs/output.csv'), header=False, index=False)

        expected = pd.DataFrame(self.expected_outcome, dtype='int')
        expected.to_csv(os.path.join(dirname, '../logs/expected_output.csv'), header=False, index=False)

        diff = (np.array(self.simulation_outcome) - np.array(self.expected_outcome)) / np.array(self.expected_outcome)

        diff_df = pd.DataFrame(diff.tolist())
        diff_df.to_csv(os.path.join(dirname, '../logs/diff.csv'), header=False, index=False)

        if self.show_plot:
            plt.close('all')

    def visualize_road(self, road):
        """
        Method drawing cars from one road
        :param road: road to draw
        """
        x = []
        y = []

        for pass_number, cars_set in enumerate([road.traffic, road.leaving_traffic]):
            for car in cars_set:
                if car.id_num == -1:
                    continue
                if car.on_two_roads:
                    if pass_number == 0:
                        # plotting front parts of split cars
                        try:
                            zero_idx = car.pos_x.index(0)
                        except ValueError:
                            zero_idx = 0
                        car_x = car.pos_x[zero_idx:]
                        car_y = car.pos_y[zero_idx:]
                    else:
                        # plotting back parts of split cars
                        try:
                            zero_idx = car.pos_x.index(0)
                        except ValueError:
                            zero_idx = 0
                        car_x = car.pos_x[:zero_idx]
                        car_y = car.pos_y[:zero_idx]
                else:
                    #  plotting cars that are in one piece
                    car_x = car.pos_x
                    car_y = car.pos_y

                # preparing matrices for plot according to the road direction
                # if road.direction == 1:
                #     x = [cx + road.x for cx in car_x]
                #     y = [cy + road.y for cy in car_y]
                # if road.direction == 2:
                #     x = [road.x - cx for cx in car_x]
                #     y = [road.y - cy for cy in car_y]
                # if road.direction == 3:
                #     x = [road.x - cy for cy in car_y]
                #     y = [road.y + cx for cx in car_x]
                # if road.direction == 4:
                #     x = [road.x + cy for cy in car_y]
                #     y = [road.y - cx for cx in car_x]
                x = []
                y = []
                for i in range(len(car_x)):
                    if car_x[i] >= road.grid_len:
                        print("ERROR")
                    _x, _y = road.calculate_car_world_position(car_x[i], car_y[i])
                    x.append(_x)
                    y.append(_y)
                # x = [road.x + car_x[i] * cos(road.direction) - car_y[i] * sin(road.direction) for i in range(len(car_x))]
                # y = [road.y + car_x[i] * sin(road.direction) + car_y[i] * cos(road.direction) for i in range(len(car_x))]

                # plotting
                self.ax.plot(x, y, marker="s", markersize=2, linestyle='-', color=car.color)

    def handle_traffic_lights(self):
        if self.time == 0:
            for intersection in self.intersections:
                intersection.perform_traffic_light_switch()
        if self.lights_period_time == NS_GREEN or self.lights_period_time == (NS_GREEN + EW_GREEN + 2):
            for intersection in self.intersections:
                intersection.switch_all_red_lights()
            # print("all red")
        if self.lights_period_time == (NS_GREEN + 2) or self.lights_period_time == 0:
            for intersection in self.intersections:
                intersection.perform_traffic_light_switch()
            # print("switch", self.intersections[0].green_light)
        self.lights_period_time = (self.lights_period_time + 1) % (NS_GREEN + EW_GREEN + 4)

    def simulation_loop(self, _):
        """
        Function performs one complete simulation step:
        lane changes
        velocities update
        positions update (with road switching)
        road visualisation
        :param _:
        """
        if self.show_plot:
            self.ax.cla()
            plt.xlim((-2, 233))
            plt.ylim((-2, 148))
            plt.gca().set_aspect('equal', adjustable='box')
            self.ax.imshow(self.background, extent=[-2, 233, -2, 148])  # 239x152
            plt.axis('off')
            # plt.grid(axis='both', alpha=10)
            # plt.grid(axis='x')

        # if self.time == 10:
        #     self.ani.event_source.stop()
        #     plt.close('all')

        if self.time % 60 == 0:
            print(f"Simulation minute {self.time//60 + 1}")
            for i, edge in enumerate(self.edges):
                # print(f"{i}: {edge.num_generated}")
                edge.reset(self.num_cars_to_generate[i][self.time // 60])

            if self.time > 0:
                # print("update")
                for i, intersection in enumerate(self.intersections):
                    self.simulation_outcome[i].append(intersection.vehicle_passed_counter)
                    intersection.vehicle_passed_counter = 0

        # resetting "car moved" flags at the beginning of iteration
        for road in self.roads + self.edge_roads:
            for car in road.traffic:
                car.moved = False
                if min(car.pos_y) < 0:
                    # print("ERROR")
                    # m = min(car.pos_y)
                    car.pos_y = [0 for y in car.pos_y]

        for edge in self.edges:
            edge.terminate_cars()
            edge.generate_cars()

        # performing lane changes
        for intersection in self.intersections:
            intersection.perform_lane_changes_intersection_level()

        # performing velocity updates
        for intersection in self.intersections + self.edge_intersections:
            intersection.update_car_velocities()

        # performing position updates
        for intersection in self.intersections + self.edge_intersections:
            intersection.update_car_positions()

        if self.show_plot:
            # visualizing roads
            for road in self.roads:
                self.visualize_road(road)

        # print("")

        self.handle_traffic_lights()

        self.time += 1

        # print(self.time)

    def start(self):
        if self.show_plot:
            mng = plt.get_current_fig_manager()
            # mng.resize(*mng.window.maxsize())
            # mng.window.wm_geometry("-0+0")
            mng.full_screen_toggle()
            plt.show()
        else:
            while self.time < self.simulation_time:
                self.simulation_loop(None)
            self.handle_close(None)
        return self.simulation_outcome
