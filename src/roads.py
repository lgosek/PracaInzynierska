from cars import MAX_VEL
import random
import numpy as np
import uuid

from math import radians, sin, cos

p = 0.15
P_CHANGE = 0.8


class Road:
    def __init__(self, x, y, grid_len, num_lanes, direction, segments=None):
        """
        Class representing a single road
        :param x: horizontal position of the road on the map
        :param y: vertical position of the road on the map
        :param grid_len: length of the road
        :param num_lanes: number of lanes on the road
        :param direction: direction of the road in degrees: 0 - eastbound, values increase counter-clockwise
        :param segments: list of segments for polyline roads
        """
        self.road_id = uuid.uuid4()
        self.x = x
        self.y = y
        self.grid_len = grid_len
        self.num_lanes = num_lanes
        self.traffic = []  # cars on the road
        self.leaving_traffic = []  # cars leaving the road, partially on a new road
        self.direction = radians(direction)
        if segments is None:
            self.segments = [(self.grid_len, self.direction)]
        else:
            self.segments = [(seg[0], radians(seg[1])) for seg in segments]

    def calculate_car_world_position(self, car_x, car_y):
        x = self.x
        y = self.y

        last_direction = self.segments[-1][1]
        for seg in self.segments:
            if car_x - seg[0] < 0:
                last_direction = seg[1]
                break
            car_x -= seg[0]
            x += seg[0] * cos(seg[1])
            y += seg[0] * sin(seg[1])

        return (
            x + car_x * cos(last_direction) - car_y * sin(last_direction),
            y + car_x * sin(last_direction) + car_y * cos(last_direction)
        )

    def add_car(self, car):
        """
        Method adding new car to the road
        :param car: car to be added
        """
        self.traffic.append(car)

    def remove_car(self, car):
        """
        Method removing car from the road
        :param car: car to be removed
        """
        self.traffic.remove(car)

    def add_leaving_car(self, car):
        """
        Method adding car that is leaving the road
        :param car: car to be added
        """
        self.leaving_traffic.append(car)

    def remove_leaving_car(self, car):
        """
        Method removing car that has finally left the road
        :param car: car to be removed
        """
        self.leaving_traffic.remove(car)

    @staticmethod
    def find_nearest_car(car, cars_on_lane, front):
        """
        Method finding a nearest car on specified lane in specified direction
        :param car: car for which we look for for a neighbor
        :param cars_on_lane: traffic on a lane on which we search
        # :param grid_len: length of the road
        :param front: flag which specifies direction of the search - front or back
        :return: nearest car, distance to the nearest car
        """
        pred = None
        min_dist = np.inf
        for c in cars_on_lane:
            if front:
                dist = (c.tail() - car.head() - 1)  # % grid_len
            else:
                dist = (car.tail() - c.head() - 1)  # % grid_len

            if 0 <= dist < min_dist:
                pred = c
                min_dist = dist

        return pred, min_dist

    @staticmethod
    def check_if_car_directly_next_to(car, cars_on_lane):
        """
        Checks if a car is directly next to the current car on the destination lane
        :param car: current car
        :param cars_on_lane: traffic on the destination lane
        :return: bool
        """
        for c in cars_on_lane:
            if len(set(car.pos_x) & set(c.pos_x)) > 0:
                return True
        return False

    def _check_lane_change(self, car, dest_lane_traffic, current_lane_traffic, direction):
        """
        Checks conditions for a lane change
        :param car: car wanting to perform lane change
        :param dest_lane_traffic: traffic on the destination lane
        :param current_lane_traffic: traffic on car's current lane
        :param direction: direction od lane change: -1 = to the right, 1 = to the left
        :return: bool - if lane change is possible
        """
        # lane changes on the las 20 meters of the road are forbidden
        if car.head() > self.grid_len - 8:
            return False

        if self.check_if_car_directly_next_to(car, dest_lane_traffic):
            return False

        pred_current, gap = self.find_nearest_car(car, current_lane_traffic, front=True)
        pred_dest, gap_o = self.find_nearest_car(car, dest_lane_traffic, front=True)
        follow_dest, gap_ob = self.find_nearest_car(car, dest_lane_traffic, front=False)

        if pred_current is None or pred_current.id_num == car.id_num:
            gap = MAX_VEL + 1

        if follow_dest is None:
            gap_ob = MAX_VEL + 1

        if pred_dest is None:
            gap_o = MAX_VEL + 1

        if direction == 1 and gap_ob > 0 and gap < car.v_temp + 1 < gap_o \
                and gap_ob > MAX_VEL and random.random() < P_CHANGE:
            return True
        elif direction == -1 and gap_ob > 0 and gap_o > car.v_temp + 1 \
                and gap_ob > MAX_VEL and random.random() < P_CHANGE:
            return True
        else:
            return False

    def perform_lane_changes(self):
        """
        Method performs lane changes for each car on the road
        :return: None
        """
        if self.num_lanes == 1:
            return

        changes = []  # holds lane change values for each car

        cars_with_backs_on_consecutive_lanes = [
            [c for c in self.traffic if c.pos_y[0] == i]
            for i in range(self.num_lanes)
        ]
        for car in self.traffic:
            current_lane = car.pos_y[-1]
            # first we check possibility of a change to the right
            if current_lane > 0 and self._check_lane_change(car,
                                                            cars_with_backs_on_consecutive_lanes[current_lane - 1],
                                                            cars_with_backs_on_consecutive_lanes[current_lane], -1):
                changes.append(-1)
            else:
                if current_lane < self.num_lanes - 1 and self._check_lane_change(car,
                                                                                 cars_with_backs_on_consecutive_lanes[
                                                                                     current_lane + 1],
                                                                                 cars_with_backs_on_consecutive_lanes[
                                                                                     current_lane], 1):
                    changes.append(1)
                else:
                    changes.append(0)

        for i, car in enumerate(self.traffic):
            car.pos_y[-1] += changes[i]
