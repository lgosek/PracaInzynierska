from copy import deepcopy

import numpy as np
from scipy.optimize import minimize, rosen_der, rosen_hess
from sklearn.metrics import r2_score

from simulation import Simulation


initial_values = [
    [                               # 0
        [[0.4, 0.2, 0.4]],
        [[0.2, 0.2, 0.6]],
        [[0.5, 0.5], [0.7, 0.3]],
        [[0.5, 0.5]]
    ],
    [                               # 1
        [[0.4, 0.4, 0.2]],
        [[0.4, 0.6]],
        [[0.3, 0.7], [0.5, 0.5]],
        [[0.6, 0.2, 0.2]]
    ],
    [                               # 2
        [[0.7, 0.3], [0.3, 0.7]],
        [[0.4, 0.2, 0.4]],
        [[0.7, 0.3], [0.3, 0.7]],
        [[0.4, 0.4, 0.2]]
    ],
    [                               # 3
        [[0.7, 0.3], [0.3, 0.7]],
        [[0.4, 0.2, 0.4]],
        [[0.7, 0.3], [0.3, 0.7]],
        [[0.4, 0.2, 0.4]]
    ],
    [                               # 4
        [[0.7, 0.3], [0.3, 0.7]],
        [[0.4, 0.2, 0.4]],
        [[0.7, 0.3], [0.3, 0.7]],
        [[0.4, 0.4, 0.2]]
    ],
    [                               # 5
        [[0.7, 0.3], [0.3, 0.7]],
        [[0.4, 0.2, 0.4]],
        [[0.7, 0.3], [0.3, 0.7]],
        [[0.4, 0.2, 0.4]]
    ],
    [                               # 6
        [[], [0.5, 0.5]],
        [[0.7, 0.2, 0.1]],
        [[0.6, 0.4]],
        [[]]
    ],
    [                               # 7
        [[0.5, 0.5], [0.6, 0.4]],
        [[0.7, 0.3]],
        [[0.4, 0.2, 0.4]],
        [[]]
    ]
]


def flatten(list_to_flatten):
    for item in list_to_flatten:
        try:
            yield from flatten(item)
        except TypeError:
            yield item


def deflatten(list_to_deflatten):
    new = deepcopy(initial_values)
    i = 0
    for inter in new:
        for road in inter:
            for lane in road:
                for j, el in enumerate(lane):
                    lane[j] = list_to_deflatten[i]
                    i += 1

    return new


class Optimizer:
    def __init__(self):
        self.simulation = None

        self.expected_outcome = [
            [24, 19, 24, 28, 17],   # 0
            [18, 26, 23, 13, 22],   # 1
            [0, 0, 0, 0, 0],        # 2
            [14, 16, 12, 7, 17],    # 3
            [58, 31, 62, 44, 24],   # 4
            [43, 30, 37, 31, 34],   # 5
            [7, 13, 13, 4, 12],     # 6
            [20, 25, 24, 35, 20],   # 7

        ]

    def error_function(self, params):
        sim = Simulation(show_plot=False)

        deflatted_params = deflatten(params)
        sim.set_params(deflatted_params)

        # for intersection in sim.intersections:
        #     for i in range(4):
        #         print(intersection.allowed_neighbourhood[i])
        #     print("")

        outcome = sim.start()

        true = list(flatten(self.expected_outcome))
        pred = list(flatten(outcome))

        # np.seterr(divide='ignore', invalid='ignore')
        # abs_diff = np.abs(true - pred)
        # # perc_abs_diff[np.isnan(perc_abs_diff)] = 0
        # # perc_abs_diff[perc_abs_diff == np.inf] = pred[perc_abs_diff == np.inf]
        #
        # ret = np.mean(np.mean(abs_diff, axis=0))
        ret = r2_score(true, pred)
        print(ret)

        return ret


    def optimize(self):
        initial_params = list(flatten(initial_values))

        best_params = minimize(self.error_function,
                               x0=np.array(initial_params),
                               method='BFGS', jac=rosen_der,
                               options={'disp': True})

        print(best_params)

        # best = deflatten(best_params)
        #
        # for inter in best:
        #     print(inter)

